package JavaClass.oops.Interfaces;

public interface Loan {

	int minBal=10000;
	
	 void apply();
	 void submitDocs();
	 void validateDocs();
	 void disburseAmount();
	 void calculateEMI();
	 void repay();
	 void close();
	
	
	
	
}
