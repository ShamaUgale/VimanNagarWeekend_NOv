package JavaClass.Basics;

public class operators {

	public static void main(String[] args) {

		System.out.println("Arithmatic ops");
		
		// +     -      *      /     %
		
		int i=89;
		int j=9;
		
		System.out.println("i+j = " + (i+j));
		System.out.println("i-j = " + (i-j));
		System.out.println("i*j = " + (i*j));
		System.out.println("i/j = " + (i/j));
		System.out.println("i%j = " + (i%j));
		
//		double d= (double)i/(double)j;
//		System.out.println(d);
		
		System.out.println("comparison and conditional ops");
// >   <   >=   <=   ==     !=
		
		
		System.out.println("i>j = " + (i>j));
		System.out.println("i<j = " + (i<j));
		
		System.out.println("i>=j = " + (i>=j));
		
		System.out.println("i<=j = " + (i<=j));
		System.out.println("i==j = " + (i==j));
		System.out.println("i!=j = " + (i!=j));
		
		
		
		System.out.println("Logical ops - AND OR NOT");
		
		
	// AND &&     OR ||       NOT !
		int age=60;
		
		System.out.println(age>=18  &&  age<60);
		
		System.out.println(! (age>=60 || age<18) );
		
		
		
		System.out.println("Increment / decrement ops ");
		// ++  , --
		
		// x++   ----> post inc ====>  use x, den do x+1
		// ++x ------> pre inc  ====>  do x+1 and use x
		
		int x=10;
		System.out.println("post increment " +   (x++));//10
		System.out.println(x);//11
		
		
		
		x=10;
		System.out.println("pre increment : "+ (++x));//11
		System.out.println(x);//11
		
		
	}

}
