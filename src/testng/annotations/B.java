package testng.annotations;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class B {

	
	@BeforeClass
	public void beforeclass(){
		System.out.println("In before class ... - B");
	}
	
	
	@Test(groups={"regression","sanity"},description="TC004 - a sample test to demonstarte tests spread in multiple classes")
	public void test4(){
		System.out.println("In test4..");
	}
	
	@Test(groups={"regression","smoke"},description="TC005 - a sample test to demonstarte tests spread in multiple classes")
	public void test5(){
		System.out.println("In test5..");
	}
	
	
	@Test(groups={"regression"},description="TC006 - a sample test to demonstarte tests spread in multiple classes")
	public void test6(){
		System.out.println("In test6..");
	}
	
}
