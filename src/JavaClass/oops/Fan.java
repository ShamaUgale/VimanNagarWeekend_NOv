package JavaClass.oops;

public class Fan {

	
	
	
	// properties
	String Mfr;
	String model;
	int noOfBlades;
	int rpm;
	
	// features
	
	public void on(){
		System.out.println("Putting the fan on");
	}
	
	public void off(){
		System.out.println("Putting the fan off");
	}
	
	public void regulate(){
		System.out.println("Regulating the fan");
	}
	
	
}
