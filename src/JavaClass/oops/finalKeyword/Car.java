package JavaClass.oops.finalKeyword;

// final class cannot be extended




public final class Car {

	int wheels;
	String Model;
	double price;
	String color;
	
	public  final void start(){
		System.out.println("Car is starting");
	}
	
	public void stop(){
		System.out.println("The car is stopping");
	}
	
	
	
	
}
