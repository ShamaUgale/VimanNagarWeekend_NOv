package selenium.checkboxes;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class cb {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
	
		
		driver.get("http://www.echoecho.com/htmlforms09.htm");
		
		
		WebElement cb1=driver.findElement(By.xpath("/html/body/div[2]/table[9]/tbody/tr/td[4]/table/tbody/tr/td/div/span/form/table[1]/tbody/tr/td/table/tbody/tr[2]/td[3]/input[1]"));
		WebElement cb2=driver.findElement(By.xpath("/html/body/div[2]/table[9]/tbody/tr/td[4]/table/tbody/tr/td/div/span/form/table[1]/tbody/tr/td/table/tbody/tr[2]/td[3]/input[2]"));
		WebElement cb3=driver.findElement(By.xpath("/html/body/div[2]/table[9]/tbody/tr/td[4]/table/tbody/tr/td/div/span/form/table[1]/tbody/tr/td/table/tbody/tr[2]/td[3]/input[3]"));

		if(cb1.isSelected()){
			System.out.println("Checkbox 1 is already checked so not checking it again");
		}else{
			cb1.click();
			System.out.println("Checkbox 1 is NOT checked so checking it again");

		}
		
		if(cb2.isSelected()){
			System.out.println("Checkbox 2 is already checked so not checking it again");
		}else{
			cb2.click();
			System.out.println("Checkbox 2 is NOT checked so checking it again");

		}
		
		if(cb3.isSelected()){
			System.out.println("Checkbox 3 is already checked so not checking it again");
		}else{
			cb3.click();
			System.out.println("Checkbox 3 is NOT checked so checking it again");

		}
		
		
		
		
		
		
	}

}
