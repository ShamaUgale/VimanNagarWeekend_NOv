package testng.annotation;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class A1 {

	//testcase id
	// description
	// pre-requisites
	//steps - actions
	//test data
	//expected results
	
	
	// actual - after actual execution 
	
	//annotations
	//@Test
	//@BeforeTest
	
	@BeforeMethod
	public void beforemethod(){
		System.out.println("In beforemethod");
	}
	
	@BeforeSuite
	public void beforeSuite(){
		System.out.println("In beforeSuite");
	}
	
	
	@BeforeTest
	public void beforeTest(){
		System.out.println("In beforeTest");
	}
	
	@BeforeClass
	public void beforeclass(){
		System.out.println("In beforeclass");
	}
	
	@Test(groups="smoke",description="TC001- this is a sample test to understant the test annottaion in testng")
	public void test1(){
		//steps - actions
		
		System.out.println("test 1");
		//expected results
	}
	
	@Test(groups={"regression","smoke","bugs"})
	public void test2(){
		System.out.println("test 2");
	}
	
	@Test(groups={"sanity","smoke", "bugs"})
	public void test3(){
		System.out.println("test 3");
	}
	
	@Test(groups={"sanity","smoke"})
	public void test4(){
		System.out.println("test 4");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
