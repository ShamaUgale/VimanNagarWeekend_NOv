package JavaClass.oops;



public class Car {

	int wheels;
	String Model;
	double price;
	String color;
	
	public Car(int wheels,String Model,double price,String color ){
		this.Model=Model;
		this.color=color;
		this.price=price;
		this.wheels=wheels;
	}
	
	
	public Car(double price ){

		this.price=price;
	
	}
	
	
	
	public void start(){
		System.out.println("Car is starting");
	}
	
	public void stop(){
		System.out.println("The car is stopping");
	}
	
	
	
	
}
