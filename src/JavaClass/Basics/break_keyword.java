package JavaClass.Basics;

public class break_keyword {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		// print the table of 2 till 10
		
		for(int i=1; i<=10;i++){
			int mul=i*2;
			if(mul>10){
				break;
			}
			System.out.println("2*"+i+"="+mul);
		}
	}

}
