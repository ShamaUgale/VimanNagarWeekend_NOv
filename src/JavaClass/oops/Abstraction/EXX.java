package JavaClass.oops.Abstraction;

public class EXX {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		HomeLoan h= new HomeLoan();
		h.apply();
		h.submitDocs();
		h.disburseAmount();
		h.calculateEMI();
		h.repay();
		h.close();
		
		
		System.out.println();
		
		EducationalLoan e= new EducationalLoan();
		e.apply();
		e.submitDocs();
		e.disburseAmount();
		e.calculateEMI();
		e.repay();
		e.close();
		
	}

}
