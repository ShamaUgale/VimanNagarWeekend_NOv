package JavaClass.oops.Abstraction;

public abstract class Loan {

	double amount;
	String AccNo;
	
	
	public void apply(){
		System.out.println("Aaplying for loan");
	}
	
	
	public abstract void submitDocs();
	
	public void disburseAmount(){
		System.out.println("Disbursed the loan amount ");
	}
	
	
	public abstract void calculateEMI();
	
	public  void repay(){
		System.out.println("Repaying the loan amount");
	}
	
	
	public  void close(){
		System.out.println("Closing the loan ");
	}
	
	
	
	
}
