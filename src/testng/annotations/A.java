package testng.annotations;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class A {

	
	//annoations
	// @Test, @BeforeTest, @AfterSuite
	
	// tcid, description, pre-req, steps, testdata, expected res, actual res, 
	
	@BeforeSuite
	public void beforeSuite(){
		System.out.println("In before suite");
	}
	
	
	@BeforeMethod
	public void beforeMethod(){
		System.out.println("In before method");
	}
	
	
	@BeforeTest
	public void beforeTest(){
		System.out.println("In before test");
	}
	
	@BeforeClass
	public void beforeClass(){
		System.out.println("In before class.. - A");
	}
	
	
	
	
	@AfterMethod
	public void afterMethod(){
		System.out.println("In after method");
	}
	
	@Test(groups={"regression","smoke","bugs"},description="TC001 - a sample test to demonstarte test annotation in testng")
	public void test1(){
		// write ur selenium code for this test case
		System.out.println("In test1..");
	}
	
	@Test(groups={"bugs"})
	public void test2(){
		// write ur selenium code for this test case
		System.out.println("In test2..");
	}
	
	@Test(groups={"bugs"})
	public void test3(){
		// write ur selenium code for this test case
		System.out.println("In test3..");
	}
	
	
}
