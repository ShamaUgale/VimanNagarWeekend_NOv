package JavaClass.Basics;

import JavaClass.oops.AccessModifiers.B;

public class ifs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
	//	A a= new A();
		
		B b= new B();
		b.p=9;
		
		
		// based on the given age, tell me if the person is eligible to work in india
		// 18-60
		
		/*
		 * if(condition){
		 * 
		 * all stms here will get executed if cond is true
		 * 
		 * 
		 * } else {
		 * 
		 * if cond is false , these stmts will get executed
		 * 
		 * }
		 */
		
		
		int age=18;
		
		if(age>=18 && age<60){
			System.out.println("The age of the person is : "+ age + " and he is eligible to work");
		}else{
			System.out.println("The age of the person is : "+ age + " and he is NOT eligible to work");
		}
		
		
		System.out.println("**********************************************");
		
		
		// given a, b, c print the largest num
		
		int a=90;
		int b=899;
		int c=899;
		
		if(a>b && a>c){
			System.out.println("A is largest");
		}else if(b>a && b>c){
			System.out.println("B is greater");
		}else if(c>a && c>b){
			System.out.println("C is greater");
		}else{
			System.out.println("None are");
		}
		
		System.out.println("************************************");
		
		// given is the percentage  (valid range is 0-100 only )
		//display the class that the percentage falls in 
		
		// fail   0-35
		//3rd class 35-50
		//2nd class 50-60
		//1st class 60-75
		//disctinction 75-100
		//invalid range  <0    >100
		
		// print if the given num is even or odd
		
		int num=98;
		
		if(num%2==0){
			System.out.println(num + " is even");
		}else {
			System.out.println(num+ " is odd");
		}
		
		
		
		
		
		
	}

}
