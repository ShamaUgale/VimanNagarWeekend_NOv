package testng.parameterization;



import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class P1 {

	@Test(groups={"regression","smoke"},dataProvider="getData")
	public void loginTest(String username, String password, boolean isValidDataSet){
		System.out.println("username : "+ username);
		System.out.println("password : "+ password);
		
		// selenium - is dashboard displayed
		boolean isDashboardDisplyed=true;
		if(isValidDataSet){
			Assert.assertEquals(isDashboardDisplyed, true, "Dashboard was expected to be displayed");
		}else{
			Assert.assertEquals(isDashboardDisplyed, false,"For invalid data set - Dashborad is not suppossed to be displayed");
		}
		System.out.println("**************************************");
	}
	
	
	@DataProvider
	public Object[][] getData(){
		Object[][] data= new Object[5][3];
		
		data[0][0]="U1";
		data[0][1]="P1";
		data[0][2]=true;
		
		data[1][0]="U2";
		data[1][1]="P2";
		data[1][2]=false;
		
		data[2][0]="U3";
		data[2][1]="P3";
		data[2][2]=false;
		
		data[3][0]="U4";
		data[3][1]="P4";
		data[3][2]=false;
		
		data[4][0]="U5";
		data[4][1]="P5";
		data[4][2]=false;
		
		return data;
	}
	
	
}
