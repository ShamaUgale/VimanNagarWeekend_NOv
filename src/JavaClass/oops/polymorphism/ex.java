package JavaClass.oops.polymorphism;

public class ex {

	
	public static void main(String[] args) {

		System.out.println("************ overloading *************");
		calc c= new calc();
		c.add(55.8, 77.9);
		c.add(65, 67, 34, 9);
		c.add("Hello", 9);
		
		
		System.out.println("******* overriding - CAR***********");
		
		Car car= new Car();
		car.start();
		car.stop();
		
		System.out.println("******* overriding - SUV***********");
		
		SUV s= new SUV();
			s.start();
		s.stop();
		
		
	}

}
