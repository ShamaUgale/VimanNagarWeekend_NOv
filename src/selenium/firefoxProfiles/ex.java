package selenium.firefoxProfiles;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

public class ex {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ProfilesIni profiles= new ProfilesIni();
	FirefoxProfile fp=	profiles.getProfile("SampleProfile");
	
	fp.setAcceptUntrustedCertificates(true);
	fp.setEnableNativeEvents(true);
//	fp.setPreference(key, value)   ---- changes the registry
	FirefoxDriver driver= new FirefoxDriver(fp);
	
		
	}

}
