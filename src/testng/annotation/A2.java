package testng.annotation;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

public class A2 {

	@Test
	public void test5(){
		System.out.println("test 5");
	}
	
	@Test
	public void test6(){
		System.out.println("test 6");
	}
	
	@Test
	public void test7(){
		System.out.println("test 7");
	}
	
	@AfterSuite
	public void afterSuite(){
		System.out.println("In aftersuite- A2");
	}
	
	
	
}
