package testng.parameterization;


import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class flipkartEx {

	ChromeDriver driver=null;
	
	
	@BeforeClass
	public void setup(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		 driver= new ChromeDriver();
		driver.get("http://www.flipkart.com");
	}
	
	
	@Test(dataProvider="searchTestData")
	public void searchTest(String ProductName) throws InterruptedException{
		
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys(ProductName);
		driver.findElement(By.xpath("//*[@id='container']/div/header/div[1]/div[2]/div/div/div[2]/form/div/div[2]/button")).click();
		
		Thread.sleep(3000);
		
	String actualProductName=	driver.findElement(By.xpath("//span[@class='W-gt5y']")).getText();
		
		Assert.assertEquals(ProductName, actualProductName,"Search product test failsed for product : " + ProductName);
		
	}
	
	
	
	@DataProvider
	public Object[][] searchTestData(){
		
		Object[][] data= new Object[3][1];
		
		data[0][0]="iphone 6s";
		
		data[1][0]="Mi Band";
		
		data[2][0]="motog3";
		
		return data;
	}
	
	
	
	
}
