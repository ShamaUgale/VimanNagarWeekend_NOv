package selenium.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class w3schools {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.get("http://www.w3schools.com/html/tryit.asp?filename=tryhtml_default");
		
		int noOfFrames=driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The no of frames : " + noOfFrames);
		
		// use the frame index 
//		driver.switchTo().frame(1);
		
		// use the id or name of the frame 
//		driver.switchTo().frame("iframeResult");
		
		
		WebElement frame= driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		
		driver.switchTo().frame(frame);
		
		WebElement txt=driver.findElement(By.xpath("/html/body/h1"));
		String text=txt.getText();
		
		
		
		
		
		System.out.println("The text is : "+text);
		
		
		
		
		
		driver.switchTo().defaultContent();
		
		
		
		
		
	}

}
