package testng.dependant;

import org.testng.Assert;
import org.testng.annotations.Test;

public class crud {

	@Test(priority=1)
	public void createUserTest(){
		System.out.println("Creating user");
		Assert.fail();
	}
	
	@Test(priority=2, dependsOnMethods="createUserTest")
	public void readUserTest(){
		System.out.println("read user");
	}
	
	@Test(priority=3,dependsOnMethods="createUserTest")
	public void editUserTest(){
		System.out.println("edit user");
	}
	
	@Test(priority=4,dependsOnMethods="createUserTest")
	public void deleteUserTest(){
		System.out.println("delete user");
	}
}
