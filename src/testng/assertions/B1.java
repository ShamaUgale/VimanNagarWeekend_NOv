package testng.assertions;


import org.testng.Assert;
import org.testng.annotations.Test;

public class B1 {

	@Test(groups="smoke",description="validate the username is correctly diplayed on the dashboard/landing page")
	public void test11(){
		System.out.println("test 11");
		// selenium - fetch the username displayed on dashboard
		String actualUsername= "Abc123";
		String expectedUserName="Abc 123";
		
		// selenium - check is username displayed
		
		
		Assert.assertTrue(true,"Username is expected to be displayed on the dashboard , but it is not");
		Assert.assertEquals(actualUsername, expectedUserName,"the username is incorrectly displayed on the dashboard");
		
	}
}
