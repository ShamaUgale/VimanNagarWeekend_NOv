package selenium.dropdowns;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class dd {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
	
		
		driver.get("https://www.commonfloor.com/agent/login?show_signup=1");
		
		WebElement cityDD=driver.findElement(By.id("city"));
		
		///////// use case 1 - select any elem in the dd
		Select sec= new Select(cityDD);
		
		sec.selectByIndex(45);
		Thread.sleep(2000);
		
		// this is the value attribute of the option element under ur dropdown
		sec.selectByValue("Ghaziabad");
		Thread.sleep(2000);
		
		sec.selectByVisibleText("Pune");
		
		
		///////////// use case 2 -- fetch all the dd elements
		
		
		
		
		List<WebElement> cities= sec.getOptions();
		
		Iterator<WebElement> it= cities.iterator();
		while(it.hasNext()){
			WebElement elem= it.next();
			System.out.println(elem.getText());
		}
		
		//////////////// use case 3 --- size of the elements
		
		System.out.println("No of cities : " + cities.size());
		
		
		/////////////// use case 4 -- fetch wats selected in the dd
		
		String selectedValue=sec.getFirstSelectedOption().getText();
		
		System.out.println("Selected option : "+ selectedValue);
		
		
		
		
		
		
		
	}

}
