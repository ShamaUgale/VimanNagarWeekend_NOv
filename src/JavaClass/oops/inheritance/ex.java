package JavaClass.oops.inheritance;

public class ex {

	
	
	public static void main(String[] args) {
		System.out.println("************* CAR **************");

		Car c= new Car();
		c.color="White";
		c.Model="Maruti 800";
		c.price=55555.99;
		c.wheels=4;
		c.start();
		c.stop();
		
		System.out.println("************* SUV **************");
		
		SUV s= new SUV();
		s.color="Silver";
		s.Model="Innova";
		s.price=99999.00;
		s.speed=777;
		s.wheels=8;
		s.start();
		s.accelerate();
		s.stop();

	}

}
