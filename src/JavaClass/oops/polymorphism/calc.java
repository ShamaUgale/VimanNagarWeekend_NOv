package JavaClass.oops.polymorphism;

public class calc {

	// implement it for mul and other 
	public void add(int a, int b){
		System.out.println("a+b : "+ (a+b));
	}
	
	public void add(int a, int b, int c){
		System.out.println("a+b+c : "+ (a+b+c));
	}
	
	public void add(String a, int b){
		System.out.println("a+b : "+ (a+b));
	}
	
	
	public void add(double a, double b){
		System.out.println("a+b : "+ (a+b));
	}
	
	
	public void add(int a, int b, int c, int d){
		System.out.println("a+b+c+d : "+ (a+b+c+d));
	}
}
